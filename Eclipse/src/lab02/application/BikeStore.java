//Zacharie Makeen - 1938801

package lab02.application;
import lab02.eclipse.*;

public class BikeStore {
	public static void main(String[] args) {
		Bicycle[] arr = new Bicycle[4];
		arr[0] = new Bicycle("S-Works", 18, 40);
		arr[1] = new Bicycle("Canyon", 24, 50);
		arr[2] = new Bicycle("Trek", 20, 60);
		arr[3] = new Bicycle("Specialized", 16, 40);
		for(int i = 0; i < 4; i++) {
			System.out.println(arr[i]);
			
		}

	}

}
